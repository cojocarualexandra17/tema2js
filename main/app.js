function addTokens(input, tokens){
    if(typeof input != 'string'){
        throw new Error('Invalid input');
    }
    if(input.length < 6){
        throw new Error('Input should have at least 6 characters');
    }   
    let a = true;

    tokens.forEach((token) => {
        Object.keys(token).forEach((k) => {
            if (k !== 'tokenName') {
                a = false;
            }
        });
        if (a == true) {
            Object.values(token).forEach((v) => {
                if (typeof v !== 'string') {
                    a = false;
                }
            });              
        }
    });
      
    if (!a) {
        throw new Error('Invalid array format');
    }
    if (input.search('...') == -1) {
        return input;
    }
  tokens.map((elem) => {
        input = input.replace('...', '${' + elem.tokenName + '}');
    });
    return input;

}


const app = {
    addTokens: addTokens
}

module.exports = app;